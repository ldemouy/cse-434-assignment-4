# Assignment 4 for CSE 434 - Computer Networking at ASU.

## Requirements

* Rust 1.39.0 or later available [here]:https://www.rust-lang.org/learn/get-started
* SQLite 3.30 or later

## Instructions

run `cargo build` and then launch the client and server from within the targets folder. 

## Description

An implementation of a message bus over a network 
