use std::net::SocketAddr;
#[derive(Debug, Eq, Clone)]
pub struct Account {
    pub name: String,
    pub password: String,
    pub token: Option<String>,
    pub time_stamp: Option<time::Timespec>,
    pub address: Option<SocketAddr>,
}

impl Account {
    pub fn new<T: Into<String>>(name: T, password: T) -> Account {
        Account {
            name: name.into(),
            password: password.into(),
            token: None,
            time_stamp: None,
            address: None,
        }
    }

    pub fn check_password<T: Into<String>>(&self, password: T) -> bool {
        if password.into() == self.password {
            return true;
        }
        false
    }

    pub fn change_password<T: Into<String>>(
        &mut self,
        old_password: T,
        new_password: T,
    ) -> Result<(), ()> {
        if self.check_password(old_password) {
            self.password = new_password.into();
            return Ok(());
        }
        Err(())
    }
}

impl PartialEq for Account {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        print!("{:?}", crate::account::Account::new("", ""));
        assert_eq!(2 + 2, 4);
    }
}
