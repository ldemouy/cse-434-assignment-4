#[derive(PartialOrd, Debug, Clone)]
pub struct Subscription {
    pub subscriber: String,
    pub publisher: String,
    pub messages: Vec<Message>,
}

impl Subscription {
    pub fn new<T: Into<String>>(subscriber: T, publisher: T) -> Subscription {
        Subscription {
            subscriber: subscriber.into(),
            publisher: publisher.into(),
            messages: vec![],
        }
    }
}

impl PartialEq for Subscription {
    fn eq(&self, other: &Self) -> bool {
        if self.publisher == other.publisher && self.subscriber == other.subscriber {
            return true;
        }
        false
    }
}

use std::time::SystemTime;
#[derive(PartialEq, PartialOrd, Debug, Clone)]
pub struct Message {
    pub publisher: String,
    pub message: String,
    pub time_stamp: SystemTime,
}
