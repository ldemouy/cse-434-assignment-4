use serde::{Deserialize, Serialize};
use std::time::SystemTime;

#[derive(Serialize, Deserialize, PartialEq, PartialOrd, Debug)]
pub enum Message {
    ResetSession {
        account: String,
        token: String,
    },
    Error {
        message: String,
    },
    NotLoggedIn,

    Register {
        user_name: String,
        password: String,
    },
    RegisterSuccess,
    RegisterFail,

    Login {
        user_name: String,
        password: String,
    },
    LoginSuccess {
        token: String,
    },
    LoginFail,

    Subscribe {
        subscriber: String,
        publisher: String,
        token: String,
    },
    SubscribeSuccess,
    SubscribeFail,

    Unsubscribe {
        subscriber: String,
        publisher: String,
        token: String,
    },
    UnsubscribeSuccess,
    UnsubscribeFail,

    Post {
        account: String,
        message: String,
        token: String,
    },
    PostAcknowledge,

    Forward {
        account: String,
        message: String,
    },
    ForwardAcknowledge,

    Retrieve {
        account: String,
        amount: usize,
        token: String,
    },
    RetrieveAcknowledge {
        account: String,
        message: String,
        time_stamp: SystemTime,
    },
    RetrieveFinish,

    Logout {
        account: String,
        token: String,
    },
    LogoutAcknowledge,
}
