use crate::message_bus::MessageBus;
use assignment_4_lib::message::Message;
use std::error::Error;
use std::net::{SocketAddr, UdpSocket};
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;

pub struct Server {
    pub socket: UdpSocket,
    bus_tx: Sender<(SocketAddr, Message)>,
    bus_rx: Receiver<(SocketAddr, Message)>,
}

impl Server {
    pub fn new(socket: UdpSocket) -> Server {
        let (tx, rx) = mpsc::channel();
        Server {
            socket,
            bus_tx: tx,
            bus_rx: rx,
        }
    }
    pub fn run(&self) {
        let socket = self.socket.try_clone().expect("couldn't clone socket");
        let mut message_bus =
            MessageBus::new(self.bus_tx.clone(), String::from("assignment_4.sqlite"));
        let tx = message_bus.bus_sender.clone();
        //Socket Listening Thread
        thread::spawn(move || loop {
            let (remote_address, message) = recv_message(&socket).expect("Could not receive");
            tx.send((remote_address, message)).expect("could not send");
        });

        //Message Bus Thread
        thread::spawn(move || message_bus.run());

        //Receive from Message bus and send over socket
        loop {
            let (address, message) = self.bus_rx.recv().expect("could not receive");
            let message = serde_json::to_string(&message).expect("Couldn't Serialize message");
            self.socket
                .send_to(message.as_bytes(), address)
                .expect("couldn't send message");
        }
    }
}

fn recv_message(socket: &UdpSocket) -> Result<(SocketAddr, Message), Box<dyn Error>> {
    const MAX_DATAGRAM_SIZE: usize = 65_507;
    let mut buf = vec![0u8; MAX_DATAGRAM_SIZE];
    let (_, remote_addr) = socket.recv_from(&mut buf).expect("couldn't receive");
    let buf = std::str::from_utf8(&buf)
        .expect("couldn't translate to utf8")
        .trim_end_matches('\0')
        .as_bytes();
    let response: Message = serde_json::from_slice(&buf).expect("Couldn't Deserialize");
    Ok((remote_addr, response))
}
