use crate::error::ErrorKind;
use assignment_4_lib::account::Account;
use assignment_4_lib::subscription::Subscription;
use rusqlite::{params, Connection, NO_PARAMS};
use std::net::SocketAddr;

pub struct Message {
    pub subscriber: String,
    pub publisher: String,
    pub message: String,
}

pub struct DatabaseManager {
    connection: Connection,
}

impl DatabaseManager {
    pub fn new<T: Into<String>>(db_location: T) -> DatabaseManager {
        let connection = Connection::open(db_location.into()).expect("Couldn't open file");
        connection
            .execute(
                "CREATE TABLE IF NOT EXISTS `Users`
                (
                    `UserName`      TEXT    NOT NULL PRIMARY KEY,
                    `Password`      TEXT    NOT NULL,
                    `Token`         TEXT    UNIQUE,
                    `TimeStamp`     TEXT    ,
                    `Address`       TEXT
                );",
                NO_PARAMS,
            )
            .expect("could not create Users table");
        connection
            .execute(
                "CREATE TABLE IF NOT EXISTS `Subscriptions`
                (
                    `Id`            INTEGER   NOT NULL PRIMARY KEY AUTOINCREMENT,
                    `Subscriber`    TEXT      NOT NULL,
                    `Publisher`     TEXT      NOT NULL
                )",
                NO_PARAMS,
            )
            .expect("Could not create Subscriptions table");
        connection
            .execute(
                "CREATE TABLE IF NOT EXISTS `Messages`
                (
                    `Id`            INTEGER   NOT NULL PRIMARY KEY AUTOINCREMENT,
                    `Subscriber`    TEXT      NOT NULL,
                    `Publisher`     TEXT      NOT NULL,
                    `Message`       TEXT      NOT NULL

                )",
                NO_PARAMS,
            )
            .expect("Could not Create Messages table");
        DatabaseManager { connection }
    }

    pub fn register<T: Into<String> + Clone>(
        &self,
        user_name: T,
        password: T,
    ) -> Result<Account, ErrorKind> {
        self.connection
            .execute(
                "INSERT OR IGNORE INTO `Users`(`UserName`, `Password`)
                VALUES (?1 , ?2);",
                params![&user_name.clone().into(), &password.clone().into()],
            )
            .expect("Could not register user");
        if let Some(result) = self.get_user(&user_name.clone().into()) {
            if result.password == password.clone().into() {
                return Ok(result);
            }
        }
        Err(ErrorKind::AccountAlreadyExists)
    }

    pub fn login<T: Into<String> + Clone>(
        &self,
        user_name: T,
        password: T,
        address: SocketAddr,
    ) -> Result<Account, ErrorKind> {
        let user = self.get_user(user_name.clone());

        match user {
            Some(user) => {
                if password.clone().into() == user.password {
                    use guid_create::GUID;
                    self.connection
                        .execute(
                            "UPDATE `Users` 
                             SET `Token` = ?1 , `TimeStamp` = ?2, `Address` = ?3
                             WHERE `UserName`= ?4;",
                            params![
                                GUID::rand().to_string(),
                                time::get_time(),
                                format!("{}", address),
                                user_name.clone().into()
                            ],
                        )
                        .expect("couldn't prepare update");
                    return Ok(self.get_user(user_name).unwrap());
                }
                Err(ErrorKind::PasswordIncorrect)
            }
            None => Err(ErrorKind::AccountDoesNotExist),
        }
    }

    pub fn logout<T: Into<String>>(&self, user_name: T) {
        self.connection
            .execute(
                "UPDATE Users
                 SET `TOKEN` = NULL, `TimeStamp` = NULL, `Address` = NULL
                 WHERE `UserName`=?1;",
                params![&user_name.into()],
            )
            .expect("could not prepare query");
    }

    pub fn is_logged_in<T: Into<String> + Clone>(&self, user_name: T) -> bool {
        let account = self.get_user(user_name.clone().into());
        if let Some(account) = account {
            if account.time_stamp.is_some()
                && time::get_time().sec - account.time_stamp.unwrap().sec < 60
            {
                self.connection
                    .execute(
                        "UPDATE `Users`
                         SET `TimeStamp` = ?1
                         WHERE `UserName` = ?2;",
                        params![time::get_time(), &user_name.clone().into()],
                    )
                    .expect("couldn't update timestamp");
                return true;
            } else {
                self.logout(user_name.into());
            }
        }
        false
    }

    pub fn get_user<T: Into<String>>(&self, user_name: T) -> Option<Account> {
        let mut cursor = self
            .connection
            .prepare(
                "SELECT * FROM `Users` 
                 WHERE `UserName` = ?1;",
            )
            .expect("Could not prepare query");
        let cursor = cursor
            .query_map(params![&user_name.into()], |row| {
                let address: Option<String> = row.get(4)?;
                Ok(Account {
                    name: row.get(0)?,
                    password: row.get(1)?,
                    token: row.get(2)?,
                    time_stamp: row.get(3)?,
                    address: if let Some(address) = address {
                        Some(address.parse().expect("Invalid Address"))
                    } else {
                        None
                    },
                })
            })
            .expect("Could not execute query");
        let mut cursor = cursor.filter_map(Result::ok);
        cursor.next()
    }

    pub fn get_logged_in_accounts(&self) -> Vec<Account> {
        let accounts = self.get_all_accounts();
        let now = time::get_time();
        accounts
            .into_iter()
            .filter(|account| {
                account.time_stamp.is_some() && now.sec - account.time_stamp.unwrap().sec < 60
            })
            .collect()
    }
    pub fn get_all_accounts(&self) -> Vec<Account> {
        let mut cursor = self
            .connection
            .prepare("SELECT * FROM `Users`;")
            .expect("couldn't prepare select");
        let cursor = cursor
            .query_map(NO_PARAMS, |row| {
                let address: Option<String> = row.get(4)?;
                Ok(Account {
                    name: row.get(0)?,
                    password: row.get(1)?,
                    token: row.get(2)?,
                    time_stamp: row.get(3)?,
                    address: if let Some(address) = address {
                        Some(address.parse().expect("Invalid Address"))
                    } else {
                        None
                    },
                })
            })
            .expect("couldn't execute query");
        cursor.filter_map(Result::ok).collect::<Vec<Account>>()
    }
    pub fn add_subscription<T: Into<String> + Clone>(
        &self,
        subscriber: T,
        publisher: T,
    ) -> Result<(), ErrorKind> {
        if self.get_user(subscriber.clone()).is_none() {
            return Err(ErrorKind::AccountDoesNotExist);
        }
        if self.get_user(publisher.clone()).is_none() {
            return Err(ErrorKind::AccountDoesNotExist);
        }
        if !self.subscription_exists(subscriber.clone(), publisher.clone()) {
            self.connection
                .execute(
                    "INSERT OR IGNORE INTO `Subscriptions` ('Subscriber', 'Publisher')
                    Values (?1, ?2);",
                    params![subscriber.into(), publisher.into()],
                )
                .expect("could not execute");
            return Ok(());
        }
        Err(ErrorKind::SubscriptionAlreadyExists)
    }
    pub fn remove_subscription<T: Into<String> + Clone>(
        &self,
        subscriber: T,
        publisher: T,
    ) -> Result<(), ErrorKind> {
        if !self.subscription_exists(subscriber.clone(), publisher.clone()) {
            return Err(ErrorKind::SubscriptionDoesNotExist);
        }
        self.connection
            .execute(
                "DELETE FROM `Subscriptions` WHERE Subscriber = ?1 AND Publisher = ?2",
                params![subscriber.into(), publisher.into()],
            )
            .expect("could not execute");
        Ok(())
    }
    pub fn subscription_exists<T: Into<String>>(&self, subscriber: T, publisher: T) -> bool {
        let mut cursor = self
            .connection
            .prepare("SELECT * FROM `Subscriptions` WHERE Subscriber = ?1 AND Publisher = ?2")
            .expect("could not prepare statement");
        let cursor = cursor
            .query_map(params![subscriber.into(), publisher.into()], |row| {
                Ok(Subscription {
                    publisher: row.get(1)?,
                    subscriber: row.get(2)?,
                    messages: vec![],
                })
            })
            .expect("couldn't execute query");
        let mut cursor = cursor.filter_map(Result::ok);
        cursor.next().is_some()
    }
    pub fn get_subscribers_to_publisher<T: Into<String>>(
        &self,
        publisher: T,
    ) -> Option<Vec<Subscription>> {
        let mut cursor = self
            .connection
            .prepare("SELECT * FROM `Subscriptions` WHERE PUBLISHER = ?1")
            .expect("Could not prepare statement");
        let cursor = cursor
            .query_map(params![publisher.into()], |row| {
                Ok(Subscription {
                    subscriber: row.get(1)?,
                    publisher: row.get(2)?,
                    messages: vec![],
                })
            })
            .expect("couldn't execute query");
        let cursor = cursor.filter_map(Result::ok);
        let mut return_val = vec![];
        for subscription in cursor {
            return_val.push(subscription);
        }
        if !return_val.is_empty() {
            return Some(return_val);
        }
        None
    }
    pub fn add_message<T: Into<String> + Clone>(
        &self,
        subscriber: T,
        publisher: T,
        message: T,
    ) -> Result<(), ErrorKind> {
        if self.get_user(subscriber.clone()).is_none() {
            return Err(ErrorKind::AccountDoesNotExist);
        }
        if self.get_user(publisher.clone()).is_none() {
            return Err(ErrorKind::AccountDoesNotExist);
        }
        self.connection
            .execute(
                "INSERT INTO `Messages` (Subscriber, Publisher, Message) Values (?1, ?2, ?3)",
                params![subscriber.into(), publisher.into(), message.into()],
            )
            .expect("Could not execute");
        Ok(())
    }
    pub fn get_messages<T: Into<String> + Clone>(&self, subscriber: T) -> Option<Vec<Message>> {
        let mut cursor = self
            .connection
            .prepare(
                "SELECT * FROM `Messages` 
                WHERE Subscriber = ?1",
            )
            .expect("Couldn't prepare");
        let cursor = cursor
            .query_map(params![subscriber.into()], |row| {
                Ok(Message {
                    subscriber: row.get(1)?,
                    publisher: row.get(2)?,
                    message: row.get(3)?,
                })
            })
            .expect("Couldn't execute");
        let cursor = cursor.filter_map(Result::ok);
        let mut result = vec![];
        for message in cursor {
            result.push(message);
        }
        if !result.is_empty() {
            return Some(result);
        }
        None
    }
    pub fn pop_n_messages<T: Into<String> + Clone>(
        &self,
        subscriber: T,
        amount: usize,
    ) -> Option<Vec<Message>> {
        let mut cursor = self
            .connection
            .prepare(
                "SELECT * FROM Messages
                 WHERE Subscriber = ?1
                 ORDER BY Id ASC Limit ?2",
            )
            .expect("could not prepare");
        let cursor = cursor
            .query_map(params![subscriber.clone().into(), amount as u32], |row| {
                Ok(Message {
                    subscriber: row.get(1)?,
                    publisher: row.get(2)?,
                    message: row.get(3)?,
                })
            })
            .expect("could not execute");
        let cursor = cursor.filter_map(Result::ok);
        let mut result = vec![];
        for message in cursor {
            result.push(message);
        }
        println!("pop_n_messages: size:{}", result.len());
        self.connection
            .execute(
                "DELETE FROM Messages 
            WHERE Subscriber = ?1
            ORDER BY Id ASC Limit ?2",
                params![subscriber.into(), amount as u32],
            )
            .expect("couldn't execute");

        if !result.is_empty() {
            return Some(result);
        }
        None
    }
}
#[allow(dead_code)]
#[cfg(test)]
mod tests {
    use crate::database_manager::DatabaseManager;
    use crate::error::ErrorKind;
    use std::fs;
    use std::path::Path;

    //Convenience Functions
    fn setup_database<T: Into<String> + Clone>(path: T) -> DatabaseManager {
        if Path::new(&path.clone().into()).exists() {
            fs::remove_file(path.clone().into()).expect("Could not remove file");
        }
        DatabaseManager::new(path.clone().into())
    }

    fn register_1_user(manager: &DatabaseManager) {
        manager.register("foo", "bar").unwrap();
    }

    fn register_2_users(manager: &DatabaseManager) {
        manager.register("foo", "bar").unwrap();
        manager.register("bar", "baz").unwrap();
    }

    fn register_4_users(manager: &DatabaseManager) {
        manager.register("foo", "bar").unwrap();
        manager.register("bar", "baz").unwrap();
        manager.register("baz", "qux").unwrap();
        manager.register("qux", "norf").unwrap();
    }

    fn login_1_success(manager: &DatabaseManager) {
        let addr = "127.0.0.1:0".parse().unwrap();
        manager.login("foo", "bar", addr).unwrap();
    }
    fn login_2_success(manager: &DatabaseManager) {
        let addr = "127.0.0.1:0".parse().unwrap();
        manager.login("foo", "bar", addr).unwrap();
        manager.login("bar", "baz", addr).unwrap();
    }
    fn login_4_success(manager: &DatabaseManager) {
        let addr = "127.0.0.1:0".parse().unwrap();
        manager.login("foo", "bar", addr).unwrap();
        manager.login("bar", "baz", addr).unwrap();
        manager.login("baz", "qux", addr).unwrap();
        manager.login("qux", "norf", addr).unwrap();
    }
    fn subscribe_1_to_1_success(manager: &DatabaseManager) {
        manager.add_subscription("foo", "bar").unwrap();
    }
    fn subscribe_1_to_2_success(manager: &DatabaseManager) {
        manager.add_subscription("foo", "bar").unwrap();
        manager.add_subscription("foo", "baz").unwrap();
    }
    fn subscribe_1_to_3_success(manager: &DatabaseManager) {
        manager.add_subscription("foo", "bar").unwrap();
        manager.add_subscription("foo", "baz").unwrap();
        manager.add_subscription("foo", "qux").unwrap();
    }
    fn subscribe_2_to_1_success(manager: &DatabaseManager) {
        manager.add_subscription("foo", "bar").unwrap();
        manager.add_subscription("baz", "bar").unwrap();
    }
    fn subscribe_3_to_1_success(manager: &DatabaseManager) {
        manager.add_subscription("foo", "bar").unwrap();
        manager.add_subscription("baz", "bar").unwrap();
        manager.add_subscription("qux", "bar").unwrap();
    }
    fn get_subscriber_count<T: Into<String>>(manager: &DatabaseManager, publisher: T) -> usize {
        let result = manager.get_subscribers_to_publisher(publisher);
        if let Some(subscriptions) = result {
            return subscriptions.len();
        }
        0
    }
    fn add_1_message(manager: &DatabaseManager) {
        manager.add_message("foo", "bar", "baz").unwrap();
    }
    fn add_3_messages(manager: &DatabaseManager) {
        manager.add_message("foo", "bar", "1").unwrap();
        manager.add_message("foo", "bar", "2").unwrap();
        manager.add_message("foo", "bar", "3").unwrap();
    }

    //Tests
    #[test]
    fn register() {
        const FILENAME: &str = "assignment_4_login_manager_register.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        let user = manager.get_user("foo");
        assert!(user.is_some());
        let user = user.unwrap();
        assert_eq!(user.name, "foo");
        assert_eq!(user.password, "bar");
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn register_fail_duplicate() {
        const FILENAME: &str = "assignment_4_login_manager_register_duplicate.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        manager
            .register("foo", "baz")
            .expect_err("should have had duplicate error");
        let user = manager.get_user("foo");
        assert!(user.is_some());
        let user = user.unwrap();
        assert_eq!(user.name, "foo");
        assert_eq!(user.password, "bar");
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn login() {
        const FILENAME: &str = "assignment_4_login_manager_login.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        login_1_success(&manager);
        assert_eq!(manager.is_logged_in("foo"), true);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn login_incorrect_password() {
        const FILENAME: &str = "assignment_4_login_manager_login_incorrect_password.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        let result = manager.login("foo", "baz", "127.0.0.0:0".parse().expect("couldn't parse"));
        assert!(result.is_err());
        assert_eq!(manager.is_logged_in("foo"), false);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }

    #[test]
    fn logout() {
        const FILENAME: &str = "assignment_4_login_manager_logout.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        login_1_success(&manager);
        manager.logout("foo");
        assert_eq!(manager.is_logged_in("foo"), false);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn is_logged_in() {
        const FILENAME: &str = "assignment_4_login_manager_is_logged_in.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(manager.is_logged_in("foo"), false);
        login_1_success(&manager);
        assert_eq!(manager.is_logged_in("foo"), true);
        manager.logout("foo");
        assert_eq!(manager.is_logged_in("foo"), false);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn time_out() {
        use std::{thread, time};

        const FILENAME: &str = "assignment_4_login_manager_timeout.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        login_1_success(&manager);
        assert_eq!(manager.is_logged_in("foo"), true);
        thread::sleep(time::Duration::from_secs(61));
        assert_eq!(manager.is_logged_in("foo"), false);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_all_accounts() {
        const FILENAME: &str = "assignment_4_login_manager_get_all_accounts.sqlite";
        let manager = setup_database(FILENAME);
        register_4_users(&manager);
        let accounts = manager.get_all_accounts();
        assert_eq!(accounts.len(), 4);
        assert_eq!(accounts[0].name, "foo");
        assert_eq!(accounts[1].name, "bar");
        assert_eq!(accounts[2].name, "baz");
        assert_eq!(accounts[3].name, "qux");

        fs::remove_file(FILENAME).expect("Could not remove file");
    }

    #[test]
    fn get_logged_in_accounts() {
        const FILENAME: &str = "assignment_4_login_manager_get_logged_in_accounts.sqlite";
        let manager = setup_database(FILENAME);
        register_4_users(&manager);
        login_2_success(&manager);
        let accounts = manager.get_logged_in_accounts();
        assert_eq!(accounts.len(), 2);
        assert_eq!(accounts[0].name, "foo");
        assert_eq!(accounts[1].name, "bar");
        fs::remove_file(FILENAME).expect("Could not remove file");
    }

    #[test]
    fn get_account() {
        const FILENAME: &str = "assignment_4_database_manager_get_account.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        let account = manager.get_user("foo").unwrap();
        assert_eq!(account.name, "foo");

        let account = manager.get_user("ban");
        assert_eq!(account.is_none(), true);

        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_subscription() {
        const FILENAME: &str = "assignment_4_login_manager_add_subscription.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        assert_eq!(get_subscriber_count(&manager, "bar"), 1);
        add_1_message(&manager);

        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_subscription_duplicate() {
        const FILENAME: &str = "assignment_4_login_manager_add_subscription_duplicate.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        assert_eq!(
            manager.add_subscription("foo", "bar").err(),
            Some(ErrorKind::SubscriptionAlreadyExists)
        );
        assert_eq!(get_subscriber_count(&manager, "bar"), 1);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_subscription_subscriber_not_exist() {
        const FILENAME: &str =
            "assignment_4_login_manager_add_subscription_subscriber_not_exist.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(
            manager.add_subscription("bar", "foo").err(),
            Some(ErrorKind::AccountDoesNotExist)
        );
        assert_eq!(get_subscriber_count(&manager, "foo"), 0);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_subscription_publisher_not_exist() {
        const FILENAME: &str =
            "assignment_4_login_manager_add_subscription_publisher_not_exist.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(
            manager.add_subscription("foo", "bar").err(),
            Some(ErrorKind::AccountDoesNotExist)
        );
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn remove_subscription() {
        const FILENAME: &str = "assignment_4_login_manager_remove_subscription.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        assert_eq!(get_subscriber_count(&manager, "bar"), 1);
        manager.remove_subscription("foo", "bar").unwrap();
        assert_eq!(get_subscriber_count(&manager, "bar"), 0);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn remove_subscription_not_exist() {
        const FILENAME: &str = "assignment_4_database_manager_remove_subscription_not_exist.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        assert_eq!(get_subscriber_count(&manager, "bar"), 0);
        assert_eq!(
            manager.remove_subscription("foo", "bar").err(),
            Some(ErrorKind::SubscriptionDoesNotExist)
        );
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_subscribers_to_publisher_none() {
        const FILENAME: &str =
            "assignment_4_database_manager_get_subscribers_to_publisher_none.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(get_subscriber_count(&manager, "foo"), 0);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_subscribers_to_publisher_1() {
        const FILENAME: &str =
            "assignment_4_database_manager_get_subscribers_to_publisher_1.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        assert_eq!(get_subscriber_count(&manager, "bar"), 1);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_subscribers_to_publisher_many() {
        const FILENAME: &str =
            "assignment_4_database_manager_get_subscribers_to_publisher_many.sqlite";
        let manager = setup_database(FILENAME);
        register_4_users(&manager);
        subscribe_3_to_1_success(&manager);
        assert_eq!(get_subscriber_count(&manager, "bar"), 3);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_message() {
        const FILENAME: &str = "assignment_4_database_manager_add_messsage.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_1_message(&manager);
        let messages = manager.get_messages("foo").unwrap();
        assert_eq!(messages.len(), 1);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_message_publisher_not_exist() {
        const FILENAME: &str =
            "assignment_4_database_manager_add_message_publisher_not_exist.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(
            manager.add_message("foo", "bar", "baz").err(),
            Some(ErrorKind::AccountDoesNotExist)
        );

        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn add_message_subscriber_not_exist() {
        const FILENAME: &str =
            "assignment_4_database_manager_add_message_subscriber_not_exist.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        assert_eq!(
            manager.add_message("bar", "foo", "baz").err(),
            Some(ErrorKind::AccountDoesNotExist)
        );
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_messages_none_no_subscription() {
        const FILENAME: &str =
            "assignment_4_database_manager_get_messages_none_no_subscription.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        let messages = manager.get_messages("foo");
        assert_eq!(messages.is_none(), true);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_messages_none_has_subscription() {
        const FILENAME: &str =
            "assignment_4_database_manager_get_messages_none_has_subscription.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        let messages = manager.get_messages("foo");
        assert_eq!(messages.is_none(), true);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_messages_1() {
        const FILENAME: &str = "assignment_4_database_manager_get_messages_1.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_1_message(&manager);
        let messages = manager.get_messages("foo");
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 1);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn get_messages_many() {
        const FILENAME: &str = "assignment_4_database_manager_get_messages_many.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_3_messages(&manager);
        let messages = manager.get_messages("foo");
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 3);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_0() {
        const FILENAME: &str = "assignment_4_database_manager_pop_message_0.sqlite";
        let manager = setup_database(FILENAME);
        register_1_user(&manager);
        let messages = manager.pop_n_messages("foo", 5);
        assert_eq!(messages.is_none(), true);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_1() {
        const FILENAME: &str = "assignment_4_database_manager_pop_message_1.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_1_message(&manager);
        let messages = manager.pop_n_messages("foo", 1);
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 1);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_many() {
        const FILENAME: &str = "assignment_4_database_manager_pop_message_many.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_3_messages(&manager);
        let messages = manager.pop_n_messages("foo", 3);
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 3);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_request_more_than_stored() {
        const FILENAME: &str =
            "assignment_4_database_manager_pop_message_request_more_than_stored.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_3_messages(&manager);
        let messages = manager.pop_n_messages("foo", 5);
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 3);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_request_less_than_stored() {
        const FILENAME: &str =
            "assignment_4_database_manager_pop_message_request_less_than_stored.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_3_messages(&manager);
        let messages = manager.pop_n_messages("foo", 2);
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 2);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn pop_message_equals_stored_amount() {
        const FILENAME: &str =
            "assignment_4_database_manager_pop_message_equals_stored_amount.sqlite";
        let manager = setup_database(FILENAME);
        register_2_users(&manager);
        subscribe_1_to_1_success(&manager);
        add_3_messages(&manager);
        let messages = manager.pop_n_messages("foo", 3);
        assert_eq!(messages.is_some(), true);
        if let Some(messages) = messages {
            assert_eq!(messages.len(), 3);
        }
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
}
