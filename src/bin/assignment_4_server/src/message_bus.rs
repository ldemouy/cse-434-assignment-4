use crate::database_manager::DatabaseManager;
use assignment_4_lib::account::Account;
use assignment_4_lib::message;
use assignment_4_lib::subscription;
use std::net::SocketAddr;
use std::sync::mpsc::{channel, Receiver, Sender};
use std::time::SystemTime;

pub struct MessageBus {
    database_manager: DatabaseManager,
    pub bus_sender: Sender<(SocketAddr, message::Message)>,
    bus_receiver: Receiver<(SocketAddr, message::Message)>,
    external_sender: Sender<(SocketAddr, message::Message)>,
}

impl MessageBus {
    pub fn new<T: Into<String>>(
        external_sender: Sender<(SocketAddr, message::Message)>,
        db_path: T,
    ) -> MessageBus {
        let (tx, rx) = channel();
        MessageBus {
            database_manager: DatabaseManager::new(db_path),
            bus_sender: tx,
            bus_receiver: rx,
            external_sender,
        }
    }

    pub fn run(&mut self) {
        loop {
            let (address, msg) = self.bus_receiver.recv().expect("could not receive");
            match msg {
                message::Message::Register {
                    user_name,
                    password,
                } => {
                    self.handle_register(address, user_name, password);
                }
                message::Message::Login {
                    user_name,
                    password,
                } => {
                    self.handle_login(address, user_name, password);
                }
                message::Message::Post {
                    account,
                    message,
                    token,
                } => {
                    self.handle_post(address, account, message, token);
                }
                message::Message::Retrieve {
                    account,
                    amount,
                    token,
                } => {
                    self.handle_retrieve(address, account, amount, token);
                }
                message::Message::Subscribe {
                    subscriber,
                    publisher,
                    token,
                } => {
                    self.handle_subscribe(address, subscriber, publisher, token);
                }
                message::Message::Unsubscribe {
                    subscriber,
                    publisher,
                    token,
                } => {
                    self.handle_unsubscribe(address, subscriber, publisher, token);
                }
                message::Message::Logout { account, token }
                | message::Message::ResetSession { account, token } => {
                    self.handle_logout(address, account, token);
                }
                _ => {}
            }
        }
    }

    pub fn register_account(&mut self, account: Account) {
        self.database_manager
            .register(account.name, account.password)
            .expect("Could not add account");
    }

    pub fn subscribe(&mut self, subscriber: Account, publisher: Account) {
        let _ = self
            .database_manager
            .add_subscription(subscriber.name, publisher.name)
            .map_err(|err| println!("{:?}", err));
    }

    pub fn unsubscribe(&mut self, subscriber: Account, publisher: Account) {
        let _ = self
            .database_manager
            .remove_subscription(subscriber.name, publisher.name)
            .map_err(|err| println!("{:?}", err));
    }

    pub fn publish(&mut self, publisher: &Account, message: subscription::Message) {
        if let Some(subscriptions) = self
            .database_manager
            .get_subscribers_to_publisher(publisher.name.clone())
        {
            println!(
                "made it into publish, with subscriber size: {}",
                subscriptions.len()
            );
            for sub in subscriptions {
                println!("subscription: {:?}", sub);
                if self.database_manager.is_logged_in(sub.subscriber.clone()) {
                    println!("forwarding");
                    let subscriber = self
                        .database_manager
                        .get_user(sub.subscriber.clone())
                        .unwrap();

                    self.external_sender
                        .send((
                            subscriber.address.unwrap(),
                            message::Message::Forward {
                                account: sub.publisher.clone(),
                                message: message.message.clone(),
                            },
                        ))
                        .expect("Could not send message");
                } else {
                    println!("storing");
                    let _ = self.database_manager.add_message(
                        sub.subscriber.clone(),
                        publisher.name.clone(),
                        message.message.clone(),
                    );
                }
            }
        }
    }

    pub fn retrieve(&mut self, subscriber: Account, amount: usize) -> Vec<subscription::Message> {
        if let Some(messages) = self
            .database_manager
            .pop_n_messages(subscriber.name, amount)
        {
            let mut result = vec![];
            for message in messages {
                result.push(subscription::Message {
                    publisher: message.publisher,
                    message: message.message,
                    time_stamp: SystemTime::now(),
                })
            }
            println!("Result length: {}", result.len());
            return result;
        }
        println!("Database Manager could not pop messages");
        vec![]
    }

    pub fn handle_register<T: Into<String> + Clone>(
        &self,
        address: SocketAddr,
        user_name: T,
        password: T,
    ) {
        if self.database_manager.register(user_name, password).is_ok() {
            self.external_sender
                .send((address, message::Message::RegisterSuccess))
                .expect("Could not send");
        } else {
            self.external_sender
                .send((address, message::Message::RegisterFail))
                .expect("could not send");
        }
    }

    pub fn handle_login<T: Into<String> + Clone>(
        &self,
        address: SocketAddr,
        user_name: T,
        password: T,
    ) {
        if let Ok(user) = self
            .database_manager
            .login(user_name, password, address.clone())
        {
            self.external_sender
                .send((
                    address,
                    message::Message::LoginSuccess {
                        token: user.token.unwrap(),
                    },
                ))
                .expect("could not send");
        } else {
            self.external_sender
                .send((address, message::Message::LoginFail))
                .expect("could not send");
        }
    }
    pub fn handle_post<T: Into<String> + Clone>(
        &mut self,
        address: SocketAddr,
        account: T,
        message: T,
        token: T,
    ) {
        if !self.database_manager.is_logged_in(account.clone()) {
            self.external_sender
                .send((address, message::Message::NotLoggedIn))
                .expect("could not send message");
            return;
        }
        let account = self.database_manager.get_user(account);
        if let Some(account) = account {
            if account.token.is_some() && account.token.clone().unwrap() == token.into() {
                self.external_sender
                    .send((address, message::Message::PostAcknowledge))
                    .expect("Could not send");
                self.publish(
                    &account,
                    subscription::Message {
                        publisher: account.name.clone(),
                        message: message.into(),
                        time_stamp: SystemTime::now(),
                    },
                );
                return;
            }
        }
    }
    pub fn handle_retrieve<T: Into<String> + Clone>(
        &mut self,
        address: SocketAddr,
        account: T,
        amount: usize,
        token: T,
    ) {
        if !self.database_manager.is_logged_in(account.clone()) {
            self.external_sender
                .send((address, message::Message::NotLoggedIn))
                .expect("could not send message");
            return;
        }
        let account = self.database_manager.get_user(account);
        if let Some(account) = account {
            if account.token.is_some() && account.token.clone().unwrap() == token.into() {
                let messages = self.retrieve(account, amount);
                for message in messages {
                    self.external_sender
                        .send((
                            address,
                            message::Message::RetrieveAcknowledge {
                                account: message.publisher,
                                message: message.message,
                                time_stamp: message.time_stamp,
                            },
                        ))
                        .expect("could not send");
                }
                self.external_sender
                    .send((address, message::Message::RetrieveFinish))
                    .expect("could not send");
            }
        }
    }
    pub fn handle_subscribe<T: Into<String> + Clone>(
        &mut self,
        address: SocketAddr,
        subscriber: T,
        publisher: T,
        token: T,
    ) {
        if !self.database_manager.is_logged_in(subscriber.clone()) {
            self.external_sender
                .send((address, message::Message::NotLoggedIn))
                .expect("could not send message");
            return;
        }
        let subscriber = self.database_manager.get_user(subscriber);
        let publisher = self.database_manager.get_user(publisher);

        if let (Some(publisher), Some(subscriber)) = (publisher, subscriber) {
            if subscriber.token.is_some() && subscriber.token.clone().unwrap() == token.into() {
                self.subscribe(subscriber, publisher);
                self.external_sender
                    .send((address, message::Message::SubscribeSuccess))
                    .expect("Could not send");
                return;
            }
        }
        self.external_sender
            .send((address, message::Message::SubscribeFail))
            .expect("could not send");
    }
    pub fn handle_unsubscribe<T: Into<String> + Clone>(
        &mut self,
        address: SocketAddr,
        subscriber: T,
        publisher: T,
        token: T,
    ) {
        if !self.database_manager.is_logged_in(subscriber.clone()) {
            self.external_sender
                .send((address, message::Message::NotLoggedIn))
                .expect("could not send message");
            return;
        }
        let subscriber = self.database_manager.get_user(subscriber);

        let publisher = self.database_manager.get_user(publisher);

        if let (Some(publisher), Some(subscriber)) = (publisher, subscriber) {
            if subscriber.token.is_some() && subscriber.token.clone().unwrap() == token.into() {
                self.unsubscribe(subscriber, publisher);
                self.external_sender
                    .send((address, message::Message::UnsubscribeSuccess))
                    .expect("could not send");
                return;
            }
        }
        self.external_sender
            .send((address, message::Message::UnsubscribeFail))
            .expect("could not send");
    }
    pub fn handle_logout<T: Into<String> + Clone>(
        &self,
        address: SocketAddr,
        account: T,
        token: T,
    ) {
        if !self.database_manager.is_logged_in(account.clone()) {
            self.external_sender
                .send((address, message::Message::NotLoggedIn))
                .expect("could not send message");
            return;
        }

        let account = self.database_manager.get_user(account.clone());
        if let Some(account) = account {
            if account.token.is_some() && account.token.clone().unwrap() == token.into() {
                self.database_manager.logout(account.name);
                self.external_sender
                    .send((address, message::Message::LogoutAcknowledge))
                    .expect("could not send message");
                return;
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::message_bus::MessageBus;
    use assignment_4_lib::message::Message;
    use std::fs;
    use std::mem;
    use std::net::SocketAddr;
    use std::path::Path;
    use std::sync::mpsc::{channel, Receiver};
    use std::time::SystemTime;
    fn setup_user_1(bus: &MessageBus, rx: &Receiver<(SocketAddr, Message)>) {
        let address = "127.0.0.1:0".parse().unwrap();
        bus.handle_register(address, "foo", "bar");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);
    }
    fn setup_user_2(bus: &MessageBus, rx: &Receiver<(SocketAddr, Message)>) {
        let address = "127.0.0.1:0".parse().unwrap();
        bus.handle_register(address, "foo", "bar");
        bus.handle_register(address, "bar", "baz");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);
    }
    fn handle_login_success<T: Into<String> + Clone>(
        user_name: T,
        password: T,
        bus: &MessageBus,
        rx: &Receiver<(SocketAddr, Message)>,
    ) -> String {
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        bus.handle_login(address.clone(), user_name, password);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::LoginSuccess {
                token: String::new()
            })
        );
        if let Message::LoginSuccess { token } = msg {
            return token;
        }
        panic!();
    }
    fn handle_login_fail<T: Into<String> + Clone>(
        user_name: T,
        password: T,
        bus: &MessageBus,
        rx: &Receiver<(SocketAddr, Message)>,
    ) {
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        bus.handle_login(address.clone(), user_name, password);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::LoginFail);
    }
    fn handle_logout_success<T: Into<String> + Clone>(
        user_name: T,
        token: T,
        bus: &MessageBus,
        rx: &Receiver<(SocketAddr, Message)>,
    ) {
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        bus.handle_logout(address, user_name, token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::LogoutAcknowledge);
    }
    #[test]
    fn register_success() {
        const FILENAME: &str = "assignment_4_message_bus_register_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn register_account_exists() {
        const FILENAME: &str = "assignment_4_message_bus_register_account_exists.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }

        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        bus.handle_register("127.0.0.1:0".parse().unwrap(), "foo", "bar");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);

        bus.handle_register("127.0.0.1:0".parse().unwrap(), "foo", "baz");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterFail);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn login_success() {
        const FILENAME: &str = "assignment_4_message_bus_login_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        handle_login_success("foo", "bar", &bus, &rx);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn login_password_incorrect() {
        const FILENAME: &str = "assignment_4_message_bus_login_password_incorrect.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        handle_login_fail("foo", "baz", &bus, &rx);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn login_user_doesnt_exist() {
        const FILENAME: &str = "assignment_4_message_bus_login_user_doesnt_exist.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        handle_login_fail("foo", "bar", &bus, &rx);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn logout() {
        const FILENAME: &str = "assignment_4_message_bus_logout.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let (tx, rx) = channel();
        let bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);
        handle_logout_success("foo", &token, &bus, &rx);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn post_success() {
        const FILENAME: &str = "assignment_4_message_bus_post_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_post(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn post_not_logged_in() {
        const FILENAME: &str = "assignment_4_message_bus_post_not_logged_in.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        bus.handle_post(address, "foo", "bar", "baz");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::NotLoggedIn);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn retrieve_empty_success() {
        const FILENAME: &str = "assignment_4_message_bus_retrieve_empty_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        bus.handle_retrieve(address, "foo", 5, &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RetrieveFinish);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn retrieve_1_success() {
        const FILENAME: &str = "assignment_4_message_bus_retrieve_1_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        handle_logout_success("foo", &token, &bus, &rx);
        let token = handle_login_success("bar", "baz", &bus, &rx);
        bus.handle_post(address, "bar", "hello world", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        handle_logout_success("bar", &token, &bus, &rx);
        let foo_token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_retrieve(address, "foo", 5, &foo_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RetrieveFinish);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn retrieve_many_success() {
        const FILENAME: &str = "assignment_4_message_bus_retrieve_many_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let foo_token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_subscribe(address, "foo", "bar", &foo_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        handle_logout_success("foo", &foo_token, &bus, &rx);
        let bar_token = handle_login_success("bar", "baz", &bus, &rx);
        bus.handle_post(address, "bar", "hello world", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world2", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world3", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world4", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world5", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world6", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        bus.handle_post(address, "bar", "hello world7", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        handle_logout_success("bar", &bar_token, &bus, &rx);
        let foo_token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_retrieve(address, "foo", 5, &foo_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world2".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world3".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world4".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            mem::discriminant(&msg),
            mem::discriminant(&Message::RetrieveAcknowledge {
                account: "bar".to_string(),
                message: "hello world5".to_string(),
                time_stamp: SystemTime::now()
            })
        );
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RetrieveFinish);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn retrieve_not_logged_in() {
        const FILENAME: &str = "assignment_4_message_bus_retrieve_not_logged_in.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        bus.handle_register(address.clone(), "foo", "bar");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);
        bus.handle_register(address.clone(), "bar", "baz");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::RegisterSuccess);
        let token = handle_login_success("foo", "bar", &bus, &rx);
        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        handle_logout_success("foo", &token, &bus, &rx);
        bus.handle_retrieve(address, "foo", 5, &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::NotLoggedIn);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }

    #[test]
    fn forward() {
        const FILENAME: &str = "assignment_4_message_bus_forward.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        let bar_token = handle_login_success("bar", "baz", &bus, &rx);

        bus.handle_post(address, "bar", "hello world", &bar_token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::PostAcknowledge);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(
            msg,
            Message::Forward {
                account: "bar".to_string(),
                message: "hello world".to_string()
            }
        );
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn subscribe_success() {
        const FILENAME: &str = "assignment_4_message_bus_subscribe_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn subscribe_publisher_doesnt_exist() {
        const FILENAME: &str = "assignment_4_message_bus_subscribe_publisher_doesnt_exist.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeFail);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn subscribe_not_logged_in() {
        const FILENAME: &str = "assignment_4_message_bus_subscribe_not_logged_in.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);

        bus.handle_subscribe(address, "foo", "bar", &String::new());
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::NotLoggedIn);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn unsubscribe_success() {
        const FILENAME: &str = "assignment_4_message_bus_unsubscribe_success.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_subscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::SubscribeSuccess);
        bus.handle_unsubscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::UnsubscribeSuccess);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[ignore]
    #[test]
    fn unsubscribe_not_subscribed() {
        const FILENAME: &str = "assignment_4_message_bus_unsubscribe_not_subscribed.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_unsubscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::UnsubscribeFail);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn unsubscribe_publisher_doesnt_exist() {
        const FILENAME: &str = "assignment_4_message_bus_unsubscribe_publisher_doesnt_exist.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_1(&bus, &rx);
        let token = handle_login_success("foo", "bar", &bus, &rx);

        bus.handle_unsubscribe(address, "foo", "bar", &token);
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::UnsubscribeFail);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
    #[test]
    fn unsubscribe_not_logged_in() {
        const FILENAME: &str = "assignment_4_message_bus_unsubscribe_not_logged_in.sqlite";
        if Path::new(FILENAME).exists() {
            fs::remove_file(FILENAME).expect("Could not remove file");
        }
        let address = "127.0.0.1:0".parse::<SocketAddr>().unwrap();
        let (tx, rx) = channel();
        let mut bus = MessageBus::new(tx, FILENAME);
        setup_user_2(&bus, &rx);

        bus.handle_unsubscribe(address, "foo", "bar", "");
        let (_, msg) = rx.recv().unwrap();
        assert_eq!(msg, Message::NotLoggedIn);
        fs::remove_file(FILENAME).expect("Could not remove file");
    }
}
