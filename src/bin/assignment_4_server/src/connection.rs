use assignment_4_lib::message::Message;
use std::env;
use std::error::Error;
use std::io::prelude::*;
use std::net::{SocketAddr, UdpSocket};
use std::os::unix::net::UnixStream;
use std::sync::mpsc;
use std::thread;
fn main() -> Result<(), Box<dyn Error>> {
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:31000".to_string());
    let recv_socket = UdpSocket::bind(&addr).expect("could not bind socket");
    println!("Listening on: {}", recv_socket.local_addr()?);
    let send_socket = recv_socket.try_clone().expect("could not clone socket");
    let (thread_sender, main_receiver) = mpsc::channel::<Vec<(SocketAddr, Message)>>();
    let (main_sender, thread_receiver) = mpsc::channel::<Vec<(SocketAddr, Message)>>();
    //udp socket listener
    thread::spawn(move || loop {
        const MAX_DATAGRAM_SIZE: usize = 65_507;
        let mut buf = vec![0u8; MAX_DATAGRAM_SIZE];
        let (_, remote_addr) = recv_socket.recv_from(&mut buf).expect("couldn't receive");
        let buf = std::str::from_utf8(&buf)
            .expect("couldn't translate to utf8")
            .trim_end_matches('\0')
            .as_bytes();
        let response: Message = serde_json::from_slice(&buf).expect("Couldn't Deserialize");
        main_sender
            .send(vec![(remote_addr, response)])
            .expect("Couldn't Send message to main thread");
    });
    thread::spawn(move || loop {
        let messages = thread_receiver.recv().expect("could not receive");
        for (address, message) in messages {
            let message = serde_json::to_string(&message).expect("Couldn't Serialize message");
            send_socket
                .send_to(message.as_bytes(), address)
                .expect("couldn't send message");
        }
    });
    let mut write_unix_socket = UnixStream::connect("/tmp/assignment_4.socket")
        .expect("could not connect to unix domain socket");
    let mut read_unix_socket = write_unix_socket.try_clone().expect("could not clone");
    thread::spawn(move || loop {
        let mut response = String::new();
        read_unix_socket.read_to_string(&mut response).unwrap();
        let messages = serde_json::from_str::<Vec<(SocketAddr, Message)>>(&response)
            .expect("could not deserialize");
        thread_sender.send(messages).expect("could not send");
    });
    loop {
        let messages = main_receiver.recv().expect("could not receive");
        let buf = serde_json::to_vec(&messages).expect("could not serialize");
        write_unix_socket
            .write_all(&buf)
            .expect("could not write to socket");
    }
}
