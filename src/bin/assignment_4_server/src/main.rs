use std::env;
use std::error::Error;
use std::net::UdpSocket;

use assignment_4_server_lib::server::Server;

fn main() -> Result<(), Box<dyn Error>> {
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "0.0.0.0:31000".to_string());

    let socket = UdpSocket::bind(addr).expect("couldn't bind to address");
    println!("Listening on: {}", socket.local_addr()?);

    let server = Server::new(socket);

    // This starts the server task.
    server.run();

    Ok(())
}
