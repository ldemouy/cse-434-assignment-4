use std::error::Error;
use std::fmt;
use std::fmt::{Display, Formatter};

#[derive(Debug, PartialEq)]
pub enum ErrorKind {
    AccountDoesNotExist,
    AccountAlreadyExists,
    SubscriptionDoesNotExist,
    SubscriptionAlreadyExists,
    PasswordIncorrect,
    TokenMismatch,
}

impl Display for ErrorKind {
    #[allow(unreachable_patterns)]
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            ErrorKind::AccountDoesNotExist => write!(f, "Account Does not exist"),
            ErrorKind::PasswordIncorrect => write!(f, "Incorrect Password"),
            ErrorKind::TokenMismatch => write!(f, "Token does not match"),
            _ => write!(f, "Unimplemented"),
        }
    }
}
impl Error for ErrorKind {}
