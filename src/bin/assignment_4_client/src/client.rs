use crate::input::{Command, IClientInput};
use crate::state::State;
use assignment_4_lib::message::Message;
use serde_json;
use std::error::Error;
use std::net::UdpSocket;
use std::sync::mpsc;
use std::thread;

pub struct Client {
    pub socket: UdpSocket,
    state: State,
    input: Box<dyn IClientInput>,
    account: String,
    token: String,
    socket_listener: mpsc::Receiver<Message>,
}

impl Client {
    pub fn new(socket: UdpSocket, input_method: Box<dyn IClientInput>) -> Client {
        let (tx, rx) = mpsc::channel();
        let listener_socket = socket.try_clone().expect("could not clone socket");
        thread::spawn(move || loop {
            let message = recv_message(&listener_socket).expect("couldn't listen");
            if let Message::Forward { account, message } = message {
                println!("<{}> - {}", account, message);
                send_message(&listener_socket, Message::ForwardAcknowledge)
                    .expect("could not send");
            } else {
                tx.send(message).expect("Could not send message");
            }
        });
        Client {
            socket,
            state: State::Offline,
            input: input_method,
            account: String::new(),
            token: String::new(),
            socket_listener: rx,
        }
    }

    pub fn run(&mut self) -> Result<(), Box<dyn Error>> {
        loop {
            match self.state {
                State::Offline => {
                    self.handle_offline()?;
                }
                State::LoginSent => {
                    self.handle_login_sent()?;
                }
                State::Online => {
                    self.handle_online()?;
                }
                State::PostSent => {
                    self.handle_post_sent()?;
                }
                State::RetrieveSent => {
                    self.handle_retrieve_sent()?;
                }
                State::LogoutSent => {
                    self.handle_logout_sent()?;
                }
                State::Quit => return Ok(()),
            }
        }
    }

    fn handle_offline(&mut self) -> Result<(), Box<dyn Error>> {
        let command = self.input.get_input();
        match command {
            Command::Register(user_name, password) => {
                send_message(
                    &self.socket,
                    Message::Register {
                        user_name,
                        password,
                    },
                )?;
                let message = self.socket_listener.recv().expect("could not receive");
                match message {
                    Message::RegisterSuccess => self.input.output(String::from("register#success")),
                    Message::RegisterFail => self.input.output(String::from("register#fail")),
                    _ => self
                        .input
                        .output(format!("Error: Unexpected Message: {:?}", message)),
                }
            }
            Command::Login(user_name, password) => {
                self.account = user_name.clone();
                send_message(
                    &self.socket,
                    Message::Login {
                        user_name,
                        password,
                    },
                )?;
                self.state = State::LoginSent;
            }
            Command::Quit => self.state = State::Quit,
            _ => self.input.output(String::from("error#must login first")),
        }
        Ok(())
    }

    fn handle_login_sent(&mut self) -> Result<(), Box<dyn Error>> {
        let message = self.socket_listener.recv().expect("could not receive");
        match message {
            Message::LoginSuccess { token } => {
                self.token = token;
                self.input.output(String::from("login_ack#successful"));
                self.state = State::Online;
            }
            Message::LoginFail => {
                self.input.output(String::from("login_ack#failed"));
                self.state = State::Offline;
            }
            _ => {
                self.input
                    .output(format!("Unexpected Message: {:?}, State: OFFLINE", message));
                self.state = State::Offline;
            }
        }
        Ok(())
    }

    fn handle_online(&mut self) -> Result<(), Box<dyn Error>> {
        let command = self.input.get_input();
        match command {
            Command::Subscribe(publisher) => {
                send_message(
                    &self.socket,
                    Message::Subscribe {
                        subscriber: self.account.clone(),
                        publisher,
                        token: self.token.clone(),
                    },
                )?;

                let message = self.socket_listener.recv().expect("could not receive");
                match message {
                    Message::SubscribeSuccess => {
                        self.input.output(String::from("subscribe_ack#successful"));
                    }
                    Message::SubscribeFail => {
                        self.input.output(String::from("subscribe_ack#failure"));
                    }
                    Message::NotLoggedIn => {
                        self.input.output(String::from("Session Timed Out"));
                        self.state = State::Offline;
                    }
                    _ => {
                        self.input
                            .output(format!("Protocol Error: Unexpected Message: {:?}", message));
                    }
                }
            }
            Command::Unsubscribe(publisher) => {
                send_message(
                    &self.socket,
                    Message::Unsubscribe {
                        subscriber: self.account.clone(),
                        publisher,
                        token: self.token.clone(),
                    },
                )?;

                let message = self.socket_listener.recv().expect("could not receive");
                match message {
                    Message::UnsubscribeSuccess => {
                        self.input
                            .output(String::from("unsubscribe_ack#successful"));
                    }
                    Message::UnsubscribeFail => {
                        self.input.output(String::from("unsubscribe_ack#failure"));
                    }
                    Message::NotLoggedIn => {
                        self.state = State::Offline;
                    }
                    _ => {
                        self.input
                            .output(format!("Protocol Error: Unexpected Message: {:?}", message));
                    }
                }
            }
            Command::Post(message) => {
                send_message(
                    &self.socket,
                    Message::Post {
                        account: self.account.clone(),
                        message,
                        token: self.token.clone(),
                    },
                )?;
                self.state = State::PostSent;
            }
            Command::Retrieve(amount) => {
                send_message(
                    &self.socket,
                    Message::Retrieve {
                        account: self.account.clone(),
                        amount,
                        token: self.token.clone(),
                    },
                )?;
                self.state = State::RetrieveSent;
            }
            Command::Logout => {
                send_message(
                    &self.socket,
                    Message::Logout {
                        account: self.account.clone(),
                        token: self.token.clone(),
                    },
                )?;
                self.state = State::LogoutSent;
            }
            Command::ResetSession => {
                send_message(
                    &self.socket,
                    Message::ResetSession {
                        account: self.account.clone(),
                        token: self.token.clone(),
                    },
                )?;
            }
            Command::Quit => self.state = State::Quit,
            _ => {
                self.input.output(String::from("Unrecognized Commands, Valid Commands are: subscribe#, unsubscribe# post# retrieve# logout#"));
            }
        }
        Ok(())
    }

    fn handle_post_sent(&mut self) -> Result<(), Box<dyn Error>> {
        let message = self.socket_listener.recv().expect("could not receive");
        match message {
            Message::PostAcknowledge => {
                self.input.output(String::from("post_ack#successful"));
                self.state = State::Online;
            }
            Message::NotLoggedIn => {
                self.input.output(String::from("Session Timed Out"));
                self.state = State::Offline;
            }
            _ => {
                self.input
                    .output(format!("Protocol Error: Unexpected Message: {:?}", message));
            }
        }
        Ok(())
    }

    fn handle_retrieve_sent(&mut self) -> Result<(), Box<dyn Error>> {
        let message = self.socket_listener.recv().expect("could not receive");
        match message {
            Message::RetrieveAcknowledge {
                account,
                message,
                time_stamp,
            } => {
                self.input
                    .output(format!("<{} : {:?}> - {}", account, time_stamp, message));
            }
            Message::RetrieveFinish => {
                self.state = State::Online;
            }
            Message::NotLoggedIn => {
                self.state = State::Offline;
            }
            _ => {
                self.input
                    .output(format!("Protocol Error: Unexpected Message: {:?}", message));
            }
        }
        Ok(())
    }

    fn handle_logout_sent(&mut self) -> Result<(), Box<dyn Error>> {
        let message = self.socket_listener.recv().expect("could not receive");
        match message {
            Message::LogoutAcknowledge => {
                self.input.output(String::from("logout_ack#successful"));
            }
            Message::NotLoggedIn => {
                self.input
                    .output(String::from("logout_ack#already logged out"));
            }
            _ => {
                self.input
                    .output(format!("Protocol Error: Unexpected Message: {:?}", message));
            }
        }
        self.state = State::Offline;
        Ok(())
    }
}

fn send_message(socket: &UdpSocket, message: Message) -> Result<(), Box<dyn Error>> {
    let message = serde_json::to_string(&message).expect("Couldn't Serialize Post");
    socket
        .send(&message.as_bytes())
        .expect("could not send message");
    Ok(())
}

fn recv_message(socket: &UdpSocket) -> Result<Message, Box<dyn Error>> {
    const MAX_DATAGRAM_SIZE: usize = 65_507;
    let mut buf = vec![0u8; MAX_DATAGRAM_SIZE];
    socket.recv(&mut buf).expect("could not receive message");
    let buf = std::str::from_utf8(&buf)
        .expect("couldn't translate to utf8")
        .trim_end_matches('\0')
        .as_bytes();
    let response: Message = serde_json::from_slice(&buf).expect("Couldn't Deserialize");
    Ok(response)
}
