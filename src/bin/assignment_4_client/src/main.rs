use assignment_4_client_lib::client::Client;
use assignment_4_client_lib::input::CommandLineInput;
use std::env;
use std::net::UdpSocket;

fn main() {
    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "127.0.0.1:31000".to_string());
    println!("{}", addr);
    let socket = UdpSocket::bind("0.0.0.0:0").expect("Could not bind");
    socket.connect(addr).expect("could not connect");
    let mut client = Client::new(socket, Box::new(CommandLineInput {}));
    client.run().expect("client failure");
}
