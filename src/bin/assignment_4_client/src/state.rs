pub enum State {
    Offline,
    LoginSent,
    Online,
    PostSent,
    RetrieveSent,
    LogoutSent,
    Quit,
}
