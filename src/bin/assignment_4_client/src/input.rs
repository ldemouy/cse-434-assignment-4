pub trait IClientInput {
    fn get_input(&self) -> Command;
    fn output(&self, output: String);
}
pub enum Command {
    Register(String, String),
    Login(String, String),
    Subscribe(String),
    Unsubscribe(String),
    Post(String),
    Retrieve(usize),
    Logout,
    ResetSession,
    Quit,
}

pub struct CommandLineInput {}

use unicode_segmentation::UnicodeSegmentation;

impl IClientInput for CommandLineInput {
    fn get_input(&self) -> Command {
        loop {
            let mut input = String::new();
            std::io::stdin()
                .read_line(&mut input)
                .expect("Failure to read stdin");
            input = input.trim().to_string();
            if input.starts_with("register#") {
                let args = input.graphemes(true).skip(9).collect::<String>();
                let args = args.split('&').collect::<Vec<&str>>();
                if args.len() == 2 {
                    return Command::Register(args[0].trim().into(), args[1].trim().into());
                }
            } else if input.starts_with("login#") {
                let args = input.graphemes(true).skip(6).collect::<String>();
                let args = args.split('&').collect::<Vec<&str>>();
                if args.len() == 2 {
                    return Command::Login(args[0].trim().into(), args[1].trim().into());
                }
            } else if input.starts_with("subscribe#") {
                let arg = input.graphemes(true).skip(10).collect::<String>();
                let arg = arg.trim().to_string();
                if !arg.is_empty() {
                    return Command::Subscribe(arg);
                } else {
                    println!("subscribe# requires an argument");
                }
            } else if input.starts_with("unsubscribe#") {
                let arg = input.graphemes(true).skip(12).collect::<String>();
                let arg = arg.trim().to_string();
                if !arg.is_empty() {
                    return Command::Unsubscribe(arg);
                } else {
                    println!("unsubscribe# requires an argument");
                }
            } else if input.starts_with("post#") {
                let arg = input.graphemes(true).skip(5).collect::<String>();
                let arg = arg.trim().to_string();
                if !arg.is_empty() {
                    return Command::Post(arg);
                } else {
                    println!("post# requires an argument");
                }
            } else if input.starts_with("retrieve#") {
                let arg = input.graphemes(true).skip(9).collect::<String>();
                let arg = arg.trim().to_string();
                if !arg.is_empty() {
                    return Command::Retrieve(
                        arg.parse::<usize>().expect("Could not parse to i32"),
                    );
                } else {
                    println!("retrieve# requires an argument");
                }
            } else if input.starts_with("logout#") {
                return Command::Logout;
            } else if input.starts_with("resetsession#") {
                return Command::ResetSession;
            } else if input.starts_with("quit#") {
                return Command::Quit;
            } else {
                println!("Error: Unrecognized Command");
            }
        }
    }

    fn output(&self, output: String) {
        println!("{}", output);
    }
}
